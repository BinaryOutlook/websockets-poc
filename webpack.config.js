const Dotenv = require('dotenv-webpack');
const TerserPlugin = require("terser-webpack-plugin");
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { resolve } = require("path");

/**
 * @type {import('webpack').Configuration}
 */
module.exports = {
    mode: process.env.ENV ?? 'production',
    output: {
      filename: './build/[name].js',
    },
    devtool: 'eval-cheap-source-map',
    module: {
      rules: [
        {
          test: /.jsx?$/,
          loader: 'babel-loader',
          exclude: /node_modules/,
          options: {
            presets: [
                ["@babel/preset-env", {
                    modules: false
                }],
                "@babel/react"
            ]
          }
        }
      ],
    },
    optimization: {
        usedExports: true,
        minimize: true,
        minimizer: [ new TerserPlugin() ],
        splitChunks: {
            chunks: 'all',
            cacheGroups: {
                vendor: {
                  test: /[\\/]node_modules[\\/](react|react-dom)[\\/]/,
                  name: 'vendor',
                },
            },
        }
    },
    plugins: [
        new Dotenv(),
        new HtmlWebpackPlugin({ templateContent: `
            <html>
              <body>
                <div id="root" />
              </body>
            </html>
      `})
    ],
    entry: "./web/index.jsx",
}