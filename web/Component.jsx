import React, { useEffect, useRef, useState } from 'react';
import { split, HttpLink, useMutation } from '@apollo/client';
import { getMainDefinition } from '@apollo/client/utilities';
import { WebSocketLink } from '@apollo/client/link/ws';
import { ApolloClient, InMemoryCache, ApolloProvider, useSubscription, gql } from '@apollo/client';

const postChat = gql`
mutation postChat($message: String!) {
  postChat(message: $message)
}`;

const newChatSub = gql`
subscription {
  newChat {
    id
    message
    time
  }
}`

const httpLink = new HttpLink({
  uri: process.env.URI || "http://localhost:3000/graphql"
});

const wsLink = new WebSocketLink({
  uri: process.env.WSURI || "ws://localhost:3000/graphql",
  options: {
    timeout: 30000,
    reconnect: true
  }
});

const splitLink = split(
  ({ query }) => {
    const definition = getMainDefinition(query);
    return (
      definition.kind === 'OperationDefinition' &&
      definition.operation === 'subscription'
    );
  },
  wsLink,
  httpLink,
);

const client = new ApolloClient({
  link: splitLink,
  cache: new InMemoryCache()
});

const MessageList = () => {
  const { data } = useSubscription(newChatSub);
  const [messages, setMessages] = useState([]);
  useEffect(()=>{
    if (data)
      {
      console.log(data);
      setMessages([
        ...messages,
        data?.newChat
      ])
    }
  },[data])
  return messages.map(message => <p key={message.id}>{message.message} - {message.time}</p>)
}

const ChatBar = () => {
  const messageRef = useRef();
  const [postMessage] = useMutation(postChat);
  return (
    <input
      style={{
        position: "-webkit-sticky",
        position: "sticky",
        top: 0
      }}
      ref={messageRef}
      onKeyUp={
        (e) => e.key === 'Enter' && postMessage({
          variables: {
            message: messageRef.current?.value
          }
        }) && (messageRef.current.value = '')
      }
    />
  )
}

const ReactComponent = () => (
  <ApolloProvider client={client}>
    <ChatBar />
    <MessageList />
  </ApolloProvider>
)

export default ReactComponent