import Component from './Component.jsx';
import ReactDOM from 'react-dom';
import React from 'react';

ReactDOM.render(<Component />, document.getElementById('root'))