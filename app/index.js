import { ApolloServer, gql, PubSub, PubSubEngine, ResolveType } from 'apollo-server-express';
import { v4 } from 'uuid';
import express from 'express';
import bodyParser from 'body-parser';
import { createServer } from 'http';
import { GraphQLScalarType, Kind } from 'graphql';
import { Readable } from 'stream';
import WebpackDevMiddleware from 'webpack-dev-middleware';
import webpack from 'webpack';
import path, { resolve } from 'path';
import config from '../webpack.config.js';

const pubsub = new PubSub();

const dateScalar = new GraphQLScalarType({
  name: 'Date',
  description: 'Date custom scalar type',
  serialize(value) {
    return value.toISOString(); // Convert outgoing Date to integer for JSON
  },
  parseValue(value) {
    return new Date(value); // Convert incoming integer to Date
  },
  parseLiteral(ast) {
    if (ast.kind === Kind.INT) {
      return new Date(parseInt(ast.value, 10)); // Convert hard-coded AST string to integer and then to Date
    }
    return null; // Invalid hard-coded value (not an integer)
  },
});

const typeDefs = gql`
type Chat {
  id: String,
  message: String,
  time: Date
}

type Subscription {
  newChat: Chat
}

type Mutation {
  postChat(message: String!): Boolean
}

type Query {
  getChats: [Chat]
}

schema {
  query: Query,
  mutation: Mutation,
  subscription: Subscription
}

scalar Date
`

/**
 * @typedef {{
 *   pubsub: PubSubEngine
 * }} Context
 */

/**
 * @param {Object} parent
 * @param {Object} input
 * @param {Context} context
 */

const postChat = (parent, input, { pubsub }, other) => {
  console.log(input);
  try {
    pubsub.publish('newChat', {
      id: v4(),
      message: input.message,
      time: new Date(),
    } )
  } catch {
    return false;
  }
  return true;
}

const newChat = {
  subscribe: () => pubsub.asyncIterator('newChat'),
  resolve: (payload) => console.log(payload) || payload,
}

/**
 * @type {ResolveType}
 */
const resolvers = {
  Query: {
    getChats: () => {
      const iterator = pubsub.asyncIterator('newChat')
      return [...((Readable.from(iterator)))];
    }
  },
  Mutation: {
    postChat,
  },
  Subscription: {
    newChat,
  },
  Date: dateScalar
}

const PORT = 3000;

const app = express();

app.use('/graphql', bodyParser.json());

const server = new ApolloServer({
  typeDefs,
  resolvers,
  context: {
    pubsub
  }
})

server.applyMiddleware({app});
if (process.env.ENV === 'development') {
  app.use(WebpackDevMiddleware(webpack(config))) 
} else {
  app.use('/', express.static(path.join(__dirname, '../dist')))
}

const httpServer = createServer(app);
server.installSubscriptionHandlers(httpServer);

httpServer.listen(PORT,() => {
  console.info("Ready and listening.");
});